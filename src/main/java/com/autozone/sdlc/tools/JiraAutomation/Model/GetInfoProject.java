package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.List;

public class GetInfoProject {

    private String key;
    private String description;
    private ProjectLead lead;
    private String id;
    private List <ProjectComponents> components;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public List getComponents() {
        return components;
    }

    public void setComponents(List components) {
        this.components = components;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public ProjectLead getLead() {
        return lead;
    }

    public void setLead(ProjectLead lead) {
        this.lead = lead;
    }

    public void setKey(String key) {
        this.key = key;
    }


    /*

 public GetInfoProject(String key) {
    }

    public GetInfoProject(String key, String id) {
        this.key = key;
        this.id = id;
    }
*/

}
