package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.Comparator;

public class ProjectLastUpdate implements Comparable<ProjectLastUpdate> {
    private String key;
    private String IssuesTotales;
    private String keyIssue;
    private String update;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getIssuesTotales() {
        return IssuesTotales;
    }

    public void setIssuesTotales(String issuesTotales) {
        IssuesTotales = issuesTotales;
    }

    public String getKeyIssue() {
        return keyIssue;
    }

    public void setKeyIssue(String keyIssue) {
        this.keyIssue = keyIssue;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }


    @Override
    public int compareTo(ProjectLastUpdate o) {
        return this.getUpdate().compareTo(o.getUpdate());

    }
}
