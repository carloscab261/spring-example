package com.autozone.sdlc.tools.JiraAutomation.controller;
import com.autozone.sdlc.tools.JiraAutomation.Model.JiraAddUsers;
import com.autozone.sdlc.tools.JiraAutomation.Model.JiraCreateProject;
import com.autozone.sdlc.tools.JiraAutomation.Model.JiraDeleteProject;
import com.autozone.sdlc.tools.JiraAutomation.Model.JiraDisableUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.autozone.sdlc.tools.JiraAutomation.services.JiraService;
import java.util.List;




@RestController

@CrossOrigin(origins = "http://localhost:3000")

public class JiraController {
    @Autowired
    JiraService jiraService;


    //Disable useres
    @RequestMapping(value = "/disableUsers", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List disableUsers(@RequestBody final JiraDisableUsers jiraServiceDisableUsers) throws Exception{
        return jiraService.disableUsers(jiraServiceDisableUsers.getList());
    }

    //Create Jira Project
    @RequestMapping(value = "/createProject", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Object createProject(@RequestBody final JiraCreateProject jiraCreateProject) throws Exception{

        return jiraService.createProject(jiraCreateProject);

    }
    //Add users
    @RequestMapping(value = "/addUsers", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String addUsers(@RequestBody final JiraAddUsers jiraAddUsers) throws Exception{
        return jiraService.addUserProject(jiraAddUsers.getKeyProject(),jiraAddUsers.getUsers());
    }

    //Delete projects
    @RequestMapping(value = "/deleteProjects", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List deleteProjects(@RequestBody final JiraDeleteProject jiraDeleteProject) throws Exception{
        return jiraService.deleteProjects(jiraDeleteProject.getKeyProjects());
    }

    //numero de issues and last update of proyects
    @RequestMapping(value = "/issuesAndLastUpdate", method= RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List issuesAndLastUpdate() throws Exception{
        return jiraService.projectLastUpdate();
    }


    @RequestMapping(value = "/test", method= RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List test() throws Exception{
        return jiraService.test();
    }



}


