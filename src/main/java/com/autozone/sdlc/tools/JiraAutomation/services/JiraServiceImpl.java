package com.autozone.sdlc.tools.JiraAutomation.services;
import com.autozone.sdlc.tools.JiraAutomation.Model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class JiraServiceImpl implements JiraService {

    @Value("${jira.api.user}")
    private String jiraApiUser;
    @Value("${jira.api.password}")
    private String jiraApiPassword;
    @Value("${jira.api.url}")
    private String jiraApiUrl;
    @Value("${jira.api.rolAdminAddProjects}")
    private String rolAdminAddProjects;
    @Value("${jira.api.userAdminDefault}")
    private String userAdminDefault;


    private GetInfoProject getInfoProject;
    private ProjectComponents projectComponents;
    private GetComponentInfo getComponentInfo;
    private GetIssues getIssues;
    private GetIssuesArray getIssuesArray;
    private ProjectLastUpdate projectLastUpdate;
    private JiraCreateProject jiraCreateProject;
    private RestTemplate restTemplate = new RestTemplate();




    public RestTemplate getRestTemplate(){
         return  this.restTemplate;
     }
    //Headers to the acces of jira
    public HttpHeaders headers(){
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(jiraApiUser, jiraApiPassword);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    //<------------------------------------------------Desactivate a list of users------------------------------------------------>

    //get project id
    public String getProjectInfo(String keyProject){
        String url = jiraApiUrl + "/rest/api/2/project/" + keyProject;
        HttpEntity request = new HttpEntity(headers());
        ResponseEntity<GetInfoProject> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetInfoProject.class);
        getInfoProject = response.getBody();
        if (response.getStatusCode() == HttpStatus.OK) {
            return getInfoProject.getId(); }
        else {
            System.out.println("Request Failed");
            return response.getStatusCode().toString();
        }
    }

    //<------Get the components of a project ------>
    public List getProjectComponents(String keyProject){
        String url = jiraApiUrl+"/rest/api/2/project/" + keyProject;
        HttpEntity request = new HttpEntity(headers());
        ResponseEntity<GetInfoProject> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetInfoProject.class
        );
        getInfoProject = response.getBody();
        if (response.getStatusCode() == HttpStatus.OK) {
            List<GetInfoProject> list = getInfoProject.getComponents();
            return list;
        } else { return Collections.singletonList("Request Failed" + response.getStatusCode()); }
    }

    //<------Get the id of all components ------>
    public List<String> getIdComponentsProject(String keyProject){
        List<String> listComponents = new ArrayList<String>();
        ObjectMapper obMaper = new ObjectMapper();
        getProjectComponents(keyProject).stream().map(post -> new JSONObject((Map) post)).forEach(json -> {
            obMaper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                projectComponents = obMaper.readValue(json.toString(), ProjectComponents.class);
                listComponents.add(projectComponents.getId());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        return listComponents;
    }

    //<------Get the lead of a component and do a verification if the user is lead of a component------>
    public String checkLeadComponent(List<String> listIdComponents, String user){
        for(String componente:listIdComponents) {
            String url = jiraApiUrl + "/rest/api/2/component/" + componente;
            HttpEntity request = new HttpEntity(headers());
            ResponseEntity<GetComponentInfo> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    request,
                    GetComponentInfo.class);
            getComponentInfo = response.getBody();
            if (response.getStatusCode() == HttpStatus.OK) {
                if (getComponentInfo.getLead() != null) {
                    if(user.equals(getComponentInfo.getLead().getKey())){
                        disableLeadComponent(componente); }
                }
            } else { return ("Request Failed"); }
        }
        return "";
    }

    //<------Change the lead of a project------>
    public GetInfoProject changeLeadProject(String keyProject) throws JSONException {
        String url = jiraApiUrl+"/rest/api/2/project/" + keyProject;
        Map<String, Object> map = new HashMap<>();
        map.put("lead", userAdminDefault);//variable en properties
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers());

        ResponseEntity<GetInfoProject> response = restTemplate.exchange(
                url,
                HttpMethod.PUT,
                request,
                GetInfoProject.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        }else { return null; }
    }

    //<------Deactivate the lead of a component---->
    public String disableLeadComponent(String idComponent) {
        String url = jiraApiUrl + "/rest/api/2/component/" + idComponent;
        //Json request
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> submap = new HashMap<>();
        submap.put("active", "false");
        map.put("lead", submap);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers());
        ResponseEntity<GetInfoProject> response = restTemplate.exchange(
                url,
                HttpMethod.PUT,
                request,
                GetInfoProject.class
        );

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getStatusCode().toString();
        } else {
            return response.getStatusCode().toString();
        }

    }

    //<------return the message error when can't deactivate a user------>
    public  boolean errorDisableUsers(String error, String compare){   //String utils
        Pattern pat = Pattern.compile(".*" + compare +".*");
        Matcher mat = pat.matcher(error);
        if (mat.matches()) {
            return true;
        }else {
            return false;
        }

    }

    //<------Get the list of keys project of user if is lead of any project or component------>
    public List extractListUserLeadProjects(String error) {
        List<String> listKeys = new ArrayList();
        for (int i = 115; i < error.length(); i++) {
            if (':' == error.charAt(i)){
                String key="";
                for (int j = i+2; j < error.length(); j++) {
                    if (',' == error.charAt(j) || '"' == error.charAt(j)){
                        listKeys.add(key);
                        if('"'== error.charAt(j)){break;}
                        j++;
                        key="";
                    }else{
                        key= key + error.charAt(j);
                    }
                }
            }
        }

        return listKeys;
    }

    //<------Desactivate a list of users------>
    public List disableUsers(List<String> listUsers) throws JSONException {

        List statusUser = new ArrayList();
        for (String usuario : listUsers) {  //User
            String url = jiraApiUrl + "/rest/api/2/user?username=" + usuario;

            //Json request
            Map<String, Object> map = new HashMap<>();
            map.put("active", "false");
            try {
                HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers());
                ResponseEntity<GetInfoProject> response = restTemplate.exchange(
                        url,
                        HttpMethod.PUT,
                        request,
                        GetInfoProject.class
                );

                if (response.getStatusCode() == HttpStatus.OK) {
                    String.valueOf(response.getBody());
                    statusUser.add("User: " + usuario + " disabled");
                } else {
                    System.out.println(response.getStatusCode());
                }

            } catch (Exception e) {

                if (errorDisableUsers(e.toString(), "Cannot deactivate user because they are currently the project lead on these projects:") == true) {
                    for (Object post : extractListUserLeadProjects(e.toString())) {
                        changeLeadProject(post.toString());
                    }
                    ArrayList<String> list = new ArrayList<String>();
                    list.add((String) usuario);
                    disableUsers(list);
                }
                if (errorDisableUsers(e.toString(), "Cannot deactivate user because they are currently a component lead on these projects:") == true) {
                    extractListUserLeadProjects(e.toString());
                    for (Object post : extractListUserLeadProjects(e.toString())) {
                        checkLeadComponent(getIdComponentsProject(post.toString()), usuario.toString());
                    }
                    ArrayList<String> list = new ArrayList<String>();
                    list.add((String) usuario);
                    disableUsers(list);
                }
                if (errorDisableUsers(e.toString(), "does not exist") == true) {
                    statusUser.add("The user" + usuario + " does not exist");
                }

            }
        }
        return statusUser;

    }

//<------------------------------------------------Delete a list of projects------------------------------------------------>
    //<------Delete a list of projects------>
    public List deleteProjects(List<String> listProjects){
        List<String> status = new ArrayList();

        for(Object project : listProjects) {

            String url = jiraApiUrl + "/rest/api/2/project/"+ project;

            HttpEntity request = new HttpEntity<>(headers());
            try {
                ResponseEntity<String> response = restTemplate.exchange(
                        url,
                        HttpMethod.DELETE,
                        request,
                        String.class
                );
                Pattern pat = Pattern.compile(".*204 NO_CONTENT.*");
                Matcher mat = pat.matcher(response.getStatusCode().toString());
                if (mat.matches()) {
                    status.add(project + " Deleted");
                }
            }catch (Exception e) {
                status.add(project + " Project does not exist ");
            }
        }
        return status;


    }

//<------------------------------------------------Create a Project------------------------------------------------>


    //<------Create a project with the configuration default------>
    public String createProject( Object informationProject){

        /*
        template KANBAN
        template SCRUM
        */
        jiraCreateProject = (JiraCreateProject) informationProject;
        String url= jiraApiUrl+"/rest/project-templates/1.0/createshared/" + getProjectInfo(jiraCreateProject.getTemplate());
        Map<String, Object> map = new HashMap<>();
        map.put("key", jiraCreateProject.getKey());
        map.put("name", jiraCreateProject.getName());
        map.put("description", jiraCreateProject.getDescription());
        map.put("lead", jiraCreateProject.getLead());
        map.put("projectTypeKey", "software");

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers());
        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    request,
                    String.class
            );
            if (response.getStatusCode() == HttpStatus.OK) {
                for (Object post:jiraCreateProject.getAddUsers()) {
                    addUserProject(jiraCreateProject.getKey(), jiraCreateProject.getAddUsers());
                }
                return String.valueOf(response.getBody());
            } else {
                return String.valueOf(response.getStatusCode());
            }
        }catch (Exception e){
            return e.toString();
        }

    }

    //<------Add a user in mode admin a project------>
    public String addUserProject(String keyProject, List listUsers){

        String url=jiraApiUrl+"/rest/api/2/project/"+keyProject+"/role/"+rolAdminAddProjects ;


        Map<String, Object> map = new HashMap<>();
        map.put("user", listUsers);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers());
        try {

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    request,
                    String.class
            );

            if (response.getStatusCode() == HttpStatus.OK) {
                return String.valueOf("Usuarios Agregados ");
            } else {
                return response.getStatusCode().toString();
            }
        }catch (Exception e){
            return  e.toString().toString();

        }

    }



//<------------------------------------------------Issues------------------------------------------------>

    //<------Get all projects------>
    public  List getAllProjects() {
        String url = jiraApiUrl+"/rest/api/2/project";

        HttpEntity  request = new HttpEntity(headers());
        ResponseEntity<List> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                List.class
        );
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return Collections.singletonList("Request Failed" + response.getStatusCode());
        }

    }
    //<------Get the key of all projects ------>

    public  List getListKeyProjects() {
        //Lista para guardar las keys de los proyectos
        List<String> list = new ArrayList<String>();
        ObjectMapper obMaper = new ObjectMapper();
        getAllProjects().stream().map(post -> new JSONObject((Map) post)).forEach(json -> {
            obMaper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                GetInfoProject project = obMaper.readValue(json.toString(), GetInfoProject.class);
                list.add(project.getKey());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        });
        return list;
    }

    //<------Get the last update of a project according to their issues------>
    public ProjectLastUpdate lastUpdateProjectIssue(String keyProject) {
        String url = jiraApiUrl+ "/rest/api/2/search?startAt=0&maxResults=1&jql=project= " + keyProject + " ORDER BY updated DESC";

        HttpEntity request = new HttpEntity(headers());

        ResponseEntity<GetIssues> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetIssues.class
        );
        //System.out.println(response.getBody());
        getIssues = response.getBody();

        if (response.getStatusCode() == HttpStatus.OK) {

            ObjectMapper obMaper = new ObjectMapper();
            getIssues.getIssues().stream().map(post -> new JSONObject((Map) post)).forEach(json -> {
                obMaper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    getIssuesArray = obMaper.readValue(json.toString(), GetIssuesArray.class);
                    //System.out.println("total issues: " + getIssues.getTotal());

                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
            projectLastUpdate = new ProjectLastUpdate();
            projectLastUpdate.setKey(keyProject);
            projectLastUpdate.setIssuesTotales(getIssues.getTotal());
            projectLastUpdate.setKeyIssue(getIssuesArray.getKey());
            projectLastUpdate.setUpdate(getIssuesArray.getFields().getUpdated());
            return  projectLastUpdate;

        } else {
           // System.out.println("Request Failed");
           // System.out.println(response.getStatusCode());

            return null;  //Objeto vacio
        }

            //return " Issues totales: "+ getIssues.getTotal() + " key issue: " + getIssuesArray.getKey() + " update: " +getIssuesArray.getFields().getUpdated();
        }

    //<------Order by the project for last update of issues------>
    public  List projectLastUpdate() {
        List list = new ArrayList();
        for (Object post: getListKeyProjects()) {
            list.add(lastUpdateProjectIssue((String) post));
        }
        Collections.sort(list);
        return list;
    }





    public List test () {

        return getIdComponentsProject("UB");

        }














}
