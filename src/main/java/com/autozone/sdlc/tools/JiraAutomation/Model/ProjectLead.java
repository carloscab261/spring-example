package com.autozone.sdlc.tools.JiraAutomation.Model;

public class ProjectLead {
    private String key;
    private String displayName;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
