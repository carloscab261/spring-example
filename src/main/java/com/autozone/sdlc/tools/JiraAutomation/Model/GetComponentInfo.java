package com.autozone.sdlc.tools.JiraAutomation.Model;

public class GetComponentInfo {
    private String id;
    private String name;
    private String active;
    private ProjectLead lead;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public ProjectLead getLead() {
        return lead;
    }

    public void setLead(ProjectLead lead) {
        this.lead = lead;
    }
}
