package com.autozone.sdlc.tools.JiraAutomation.services;


import org.json.JSONException;

import java.util.List;

public interface JiraService {

    List disableUsers(List<String> listaUsuarios ) throws JSONException;
    List deleteProjects(List<String> listProjects);
    String createProject(Object createProject);
    String addUserProject(String keyProject, List listUsers);
    List projectLastUpdate();
    List test();



}
