package com.autozone.sdlc.tools.JiraAutomation.services;
import com.autozone.sdlc.tools.JiraAutomation.Model.GetInfoProject;
import com.autozone.sdlc.tools.JiraAutomation.Model.ProjectLastUpdate;
import com.autozone.sdlc.tools.JiraAutomation.Model.JiraCreateProject;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.manyTimes;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = JiraServiceImpl.class)
@RestClientTest
class JiraServiceImplTest {

    @Value("${jira.api.url}")
    private String jiraApiUrl;
    @Value("${jira.api.rolAdminAddProjects}")
    private String rolAdminAddProjects;


    @Autowired
    private JiraServiceImpl jiraServiceImpl;
    private MockRestServiceServer server;



    @Test
    void headers() {
        String jiraApiUser = "user";
        String jiraApiPassword = "password";
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(jiraApiUser, jiraApiPassword);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        jiraServiceImpl = mock(JiraServiceImpl.class);
        when(jiraServiceImpl.headers()).thenReturn(headers);
        HttpHeaders response = jiraServiceImpl.headers();
        Assert.assertEquals(response.toString(), "[Authorization:\"Basic dXNlcjpwYXNzd29yZA==\", Content-Type:\"application/json\", Accept:\"application/json\"]");


    }
    @Test
    void getProjectInfo() {

        String project = "LOZ";
        String expectId = "11701";
        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl + "/rest/api/2/project/" + project)).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"id\": \"11701\"}", MediaType.APPLICATION_JSON));
        String actualId = jiraServiceImpl.getProjectInfo(project);
        server.verify();
        Assert.assertEquals(actualId, expectId);
    }

    @Test
    void getProjectComponents() {
        String project = "TESTA";
        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl + "/rest/api/2/project/" + project)).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"components\": [{\"id\": \"13000\"}]}", MediaType.APPLICATION_JSON));


        Map<String, Object> expectId = new HashMap<>();
        expectId.put("id", "13000");


        List actualId = jiraServiceImpl.getProjectComponents(project);
        server.verify();
        Assert.assertEquals(expectId, actualId.get(0));

    }


    @Test
    void getIdComponentsProject() {
        String project="ZOL";

        List expect= new ArrayList();
        expect.add("21312");

            JiraServiceImpl jiraServiceImplMock= mock(JiraServiceImpl.class);

            when(jiraServiceImplMock.getIdComponentsProject(project)).thenReturn(expect);

            List<String> response = jiraServiceImplMock.getIdComponentsProject(project);
            assertThat(response).isEqualTo(expect);

    }

    @Test
    void checkLeadComponent() {

        List component = new ArrayList();
        component.add("165465");
        String body="{ \"id\": \" \"}";

        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl + "/rest/api/2/component/" + component.get(0))).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(body, MediaType.APPLICATION_JSON));
        String actual =  jiraServiceImpl.checkLeadComponent(component,"123");
        server.verify();
        Assert.assertEquals("",actual);

    }

    @Test
    void changeLeadProject() throws JSONException {
        String project="BAS";
        String body="{\n" +
                "    \"id\": \"10215\",\n" +
                "    \"lead\": {\n" +
                "        \"key\": \"10058743\"" +
                "    }" +

                "}";

        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl +"/rest/api/2/project/" + project)).andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess(body, MediaType.APPLICATION_JSON));

        GetInfoProject actual=  jiraServiceImpl.changeLeadProject(project);
        GetInfoProject  expected= new GetInfoProject();
        expected.setId("10215");

        server.verify();

        Assert.assertEquals(expected.getId(),actual.getId());

    }

    @Test
    void disableLeadComponent() {
        String idComponent="10215";
        String body="{\n" +
                "    \"id\": \"10215\",\n" +
                "    \"lead\": {\n" +
                "        \"key\": \"10058743\"" +
                "    }" +

                "}";

        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl +"/rest/api/2/component/" + idComponent)).andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess(body, MediaType.APPLICATION_JSON));

        String actual=  jiraServiceImpl.disableLeadComponent(idComponent);
        String expected="200 OK";

        server.verify();

        Assert.assertEquals(expected,actual);
    }

    @Test
    void errorDisableUsers() {
        String error="Cannot deactivate user because they are currently the project lead on these projects:";
        String comparar="Cannot deactivate user because they are currently the project lead on these projects:";


        JiraServiceImpl jiraServiceImplMock= mock(JiraServiceImpl.class);

        when(jiraServiceImplMock.errorDisableUsers(error,comparar)).thenReturn(true);

        boolean response = jiraServiceImplMock.errorDisableUsers(error,comparar);
        assertThat(response).isEqualTo(true);
    }

    @Test
    void extractListUserLeadProjects() {
        String error="Cannot deactivate user because they are currently the project lead on these projects: OCD";

        List projects = new ArrayList();
        projects.add("OCD");

        JiraServiceImpl jiraServiceImplMock= mock(JiraServiceImpl.class);

        when(jiraServiceImplMock.extractListUserLeadProjects(error)).thenReturn(projects);

         List response = jiraServiceImplMock.extractListUserLeadProjects(error);

        assertThat(response.get(0)).isEqualTo("OCD");

    }

    @Test
    void disableUsers() throws JSONException {
        List user= new ArrayList();
        user.add("1105789");
        String body="{\n" +
                "    \"key\": \"JIRAUSER163970\",\n" +
                "    \"emailAddress\": \"carlos.cabrera@autozone.com\",\n" +
                "    \"displayName\": \"Carlos Cabrera\",\n" +
                "    \"active\": true" +
                "}";
        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl +"/rest/api/2/user?username=" + user.get(0))).andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess(body, MediaType.APPLICATION_JSON));
        List actual=  jiraServiceImpl.disableUsers(user);
        List expected = new ArrayList();
        expected.add("User: 1105789 disabled");
        server.verify();
        Assert.assertEquals(expected,actual);
    }

    @Test
    void deleteProjects() {
        List projects = new ArrayList();
        projects.add("TESTA");
        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl + "/rest/api/2/project/" + projects.get(0))).andExpect(method(HttpMethod.DELETE))
                .andRespond(withSuccess("", MediaType.APPLICATION_JSON));


        List expect = new ArrayList();
        List actual= jiraServiceImpl.deleteProjects(projects);
        server.verify();
        Assert.assertEquals(expect, actual);

    }

    @Test
    void createProject() {

        String expect="{\"projectKey\": \"EJE11\"}";



        List users = new ArrayList();
        users.add("10023458");
        users.add("10033995");
        JiraCreateProject infoProject = new JiraCreateProject();

        infoProject .setKey("TESTB");
        infoProject .setDescription("TEST-AUTOMATION");
        infoProject .setLead("ent-jira-admin");
        infoProject .setName("TEST-AUTOMATIONB");
        infoProject .setTemplate("LOZ");
        infoProject .setAddUsers(users);



        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();

        server.expect(manyTimes(), requestTo(jiraApiUrl + "/rest/api/2/project/" + infoProject .getTemplate())).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"id\": \"11701\"}", MediaType.APPLICATION_JSON));



        server.expect(manyTimes(), requestTo( jiraApiUrl +"/rest/project-templates/1.0/createshared/11701" )).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"projectKey\": \"EJE11\"}", MediaType.APPLICATION_JSON));

        server.expect(manyTimes(), requestTo(jiraApiUrl+"/rest/api/2/project/"+infoProject .getKey()+"/role/"+rolAdminAddProjects )).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("", MediaType.APPLICATION_JSON));


        String actual = jiraServiceImpl.createProject(infoProject );
        server.verify();
        Assert.assertEquals(expect , actual);


    }

    @Test
    void addUserProject() {

        String project="10215";
        List users = new ArrayList();
        users.add("115123");

        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl+"/rest/api/2/project/"+project+"/role/"+rolAdminAddProjects )).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("", MediaType.APPLICATION_JSON));
        String actual=  jiraServiceImpl.addUserProject(project, users);
        String expected="Usuarios Agregados ";
        server.verify();

        Assert.assertEquals(expected,actual);
    }

    @Test
    void getAllProjects() {

        List body= new ArrayList();
        body.add("{\"key\": \"PFCSR\"}");

        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();
        server.expect(manyTimes(), requestTo(jiraApiUrl+"/rest/api/2/project")).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(String.valueOf(body), MediaType.APPLICATION_JSON));
        List actual=  jiraServiceImpl.getAllProjects();


        Map<String, Object> expectJSON = new HashMap<>();
        expectJSON.put("key", "PFCSR");

        List expected = new ArrayList();
        expected.add(expectJSON);
        server.verify();

        Assert.assertEquals(expected,actual);
    }

    @Test
    void getListKeyProjects() {
        String error="Cannot deactivate user because they are currently the project lead on these projects: OCD";

        List projects = new ArrayList();
        projects.add("LOD");

        JiraServiceImpl jiraServiceImplMock= mock(JiraServiceImpl.class);

        when(jiraServiceImplMock.getListKeyProjects()).thenReturn(projects);

        List response = jiraServiceImplMock.getListKeyProjects();

        assertThat(response.get(0)).isEqualTo("LOD");

    }

    @Test
    void lastUpdateProjectIssue() {
        String project = "TESTA";
        String body="{\"total\": 60,\"issues\": [{\"id\": \"182318\",\"key\": \"ZBSM-60\",\"fields\": {\"updated\": \"2022-01-06T00:14:00.000-0600\"}}]}";
        MockRestServiceServer server = MockRestServiceServer.bindTo(jiraServiceImpl.getRestTemplate()).build();

        server.expect(manyTimes(), requestTo( jiraApiUrl +"/rest/api/2/search?startAt=0&maxResults=1&jql=project%3D%20"+project+"%20ORDER%20BY%20updated%20DESC")).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(body, MediaType.APPLICATION_JSON));

        ProjectLastUpdate expect = new ProjectLastUpdate();
        expect.setIssuesTotales("60");
        expect.setKey(project);;
        expect.setUpdate("2022-01-06T00:14:00.000-0600");
        expect.setKeyIssue("ZBSM-60");
        ProjectLastUpdate actual = jiraServiceImpl.lastUpdateProjectIssue(project);
        server.verify();
        Assert.assertEquals(expect.getKey(), actual.getKey());
        Assert.assertEquals(expect.getIssuesTotales(), actual.getIssuesTotales());
        Assert.assertEquals(expect.getKeyIssue(), actual.getKeyIssue());
        Assert.assertEquals(expect.getUpdate(), actual.getUpdate());


    }

}